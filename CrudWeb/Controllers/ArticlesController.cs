﻿using DataAccessLibrary.Models;
using Microsoft.AspNetCore.Mvc;

namespace CrudWeb.Controllers
{
    public class ArticlesController : Controller
    {
        private IDataRepository repository;
        public ArticlesController(IDataRepository repo)
        {
            repository = repo;
        }

        public IActionResult Index()
        {
            var articles = repository.GetAllArticles();
            return View(articles);
        }

        public IActionResult Details(int id)
        {
            var article = repository.GetArticle(id);
            if (article == null || !article.Published)
            {
                return NotFound();
            }
            return View(article);
        }

        public IActionResult Create()
        {
            ViewBag.CreateMode = true;
            return View("Editor", new Article());
        }

        [HttpPost]
        public IActionResult Create(Article article)
        {
            repository.CreateArticle(article);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Edit(int id)
        {
            ViewBag.CreateMode = false;
            return View("Editor", repository.GetArticle(id));
        }

        [HttpPost]
        public IActionResult Edit(Article article, Article original)
        {
            repository.UpdateArticle(article, original);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            repository.DeleteArticle(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
