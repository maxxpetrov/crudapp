﻿using System;

namespace DataAccessLibrary.Models
{
    public class Article
    {
        private DateTime _createdAt;
        private DateTime _updatedAt;

        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public DateTime CreatedAt => _createdAt;
        public DateTime UpdatedAt => _updatedAt;
        public bool Published { get; set; }

    }
}
