﻿using DataAccessLibrary.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary.Models
{
    public class EFDataRepository : IDataRepository
    {
        private ArticleContext context;
        public EFDataRepository(ArticleContext ctx)
        {
            context = ctx;
        }


        public IEnumerable<Article> GetAllArticles()
        {
            return context.Articles;
        }

        public Article GetArticle(int id)
        {
            return context.Articles.Find(id);
        }

        public void CreateArticle(Article newArticle)
        {
            newArticle.ID = 0;
            context.Articles.Add(newArticle);
            context.SaveChanges();
        }

        public void UpdateArticle(Article changedArticle, Article originalArticle = null)
        {
            if (originalArticle == null)
            {
                originalArticle = context.Articles.Find(changedArticle.ID);
            }
            else
            {
                context.Articles.Attach(originalArticle);
            }
            originalArticle.ID = changedArticle.ID;
            originalArticle.Title = changedArticle.Title;
            originalArticle.Description = changedArticle.Description;
            originalArticle.Text = changedArticle.Text;
            originalArticle.Published = changedArticle.Published;
            context.SaveChanges();
        }

        public void DeleteArticle(int id)
        {
            context.Articles.Remove(new Article { ID = id });
            context.SaveChanges();
        }
    }
}
