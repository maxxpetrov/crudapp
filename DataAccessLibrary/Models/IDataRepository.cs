﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary.Models
{
    public interface IDataRepository
    {
        Article GetArticle(int id);
        IEnumerable<Article> GetAllArticles();
        void CreateArticle(Article newArticle);
        void UpdateArticle(Article changedArticle, Article originalArticle = null);
        void DeleteArticle(int id);
    }
}
